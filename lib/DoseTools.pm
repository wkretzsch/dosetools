package DoseTools;

# wkretzsch@gmail.com          26 Sep 2013
our $VERSION = "0.003";
$VERSION = eval $VERSION;

=head1 CHANGELOG

v0.003 Removed more errors introduced by misguided perltidy

v0.002 Added hard call option for vcf2dose

v0.001 First working version

=cut

use 5.006;
use strict;
use warnings;
use Carp;
use File::Basename;
use Moose;
use Digest::MD5 qw(md5_hex);

has 'sampleLists' =>
  ( is => 'rw', isa => 'HashRef[Bool]', default => sub { {} } );
has 'dm'                  => ( is => 'ro', isa => 'DM',  required => 1 );
has 'doseKeepSampPL'      => ( is => 'ro', isa => 'Str', required => 1 );
has 'doseKeepSnpsPL'      => ( is => 'ro', isa => 'Str', required => 1 );
has 'vcf2dosePL'          => ( is => 'ro', isa => 'Str', required => 1 );
has 'hapGen2dosePL'       => ( is => 'ro', isa => 'Str', required => 1 );
has 'sampListIntersectPL' => ( is => 'ro', isa => 'Str', required => 1 );
has '_filterSampLists'    => (
    is      => 'rw',
    isa     => 'HashRef[Bool]',
    default => sub { {} },
);

# holds the sample list that is the intersect of all sample lists
has 'intersectSampList' =>
  ( is => 'ro', isa => 'Str', writer => '_set_intersectSampList' );

# holds all the site lists that are created on the fly
has '_filterSiteLists' => (
    is      => 'rw',
    isa     => 'HashRef[Bool]',
    default => sub { {} },
);

has '_vcfs' => (
    is      => 'rw',
    isa     => 'HashRef[Bool]',
    default => sub { {} },
);

around 'intersectSampList' => sub {
    my $orig      = shift;
    my $self      = shift;
    my $iSampList = $self->$orig;
    return $iSampList if defined $iSampList;

    my $dm        = $self->dm;
    my @sampLists = ( $self->getFilterSampLists );
    my @names     = map { basename( $_, '.sample.list' ) } @sampLists;

    my $digest = md5_hex( join( '.', @names ));
    $iSampList = "intersect.".$digest . ".sample.list";
    my $cmd =
        $self->sampListIntersectPL
      . ' -i -- '
      . join( ' ', @sampLists )
      . " > $iSampList ";
    $dm->addRule( $iSampList, \@sampLists, $cmd );
    $self->_set_intersectSampList($iSampList);
    return $self->$orig;
};

sub addFilterSiteList {
    my $self        = shift;
    my %args        = @_;
    my $rhSiteLists = $self->_filterSiteLists;
    my @siteLists   = ( $self->_arg2array( $args{filterSiteList} ) );
    confess " filterSiteList needs to be passed in " unless @siteLists;
    for my $sL (@siteLists) {
        $rhSiteLists->{$sL} = 1;
    }
    $self->_filterSiteLists($rhSiteLists);
}

sub _arg2array {
    my $self = shift;
    my $arg  = shift;
    my @out;
    if ( ref($arg) eq 'ARRAY' ) {
        push @out, @{$arg};
    }
    elsif ($arg) {
        push @out, $arg;
    }
    return @out;
}

sub addFilterSamp {
    my $self        = shift;
    my %args        = @_;
    my $rhSampLists = $self->_filterSampLists;
    my @sampLists   = (
        $self->_getFilterSampLists(
            sample   => $args{filterSample},
            sampList => $args{filterSampList}
        )
    );
    for my $sL (@sampLists) {
        $rhSampLists->{$sL} = 1;
    }
    $self->_filterSampLists($rhSampLists);
}

sub getFilterSampLists {
    my $self = shift;
    return ( $self->_getFilterSampLists );
}

sub sample2sampleList {

    my $self   = shift;
    my $sample = shift;
    return $sample unless $sample;
    my $dm = $self->dm;

    my $rhSampLists = $self->sampleLists;
    my $sampList = basename( $sample, ".sample" ) . ".sample.list";

    unless ( exists $rhSampLists->{$sampList} ) {
        $dm->addRule( $sampList, $sample,
                " tail -n +3 $sample "
              . q/ | awk '{print $1}'/
              . " > $sampList " );
        $rhSampLists->{$sampList} = 1;
    }

    return $sampList;
}

# return saved lists if no lists are passed in
sub _getFilterSampLists {

    my $self     = shift;
    my %args     = @_;
    my $sample   = $args{sample} || 0;
    my $sampList = $args{sampList} || 0;

    my %sampLists;
    if ( ref($sample) eq 'ARRAY' ) {
        for my $samp ( @{$sample} ) {
            my $sL = $self->sample2sampleList($samp);
            $sampLists{$sL} = 1;
        }
    }
    elsif ($sample) {
        my $sL = $self->sample2sampleList($sample);
        $sampLists{$sL} = 1;
    }

    if ( ref($sampList) eq 'ARRAY' ) {
        for my $sL ( @{$sampList} ) {
            $sampLists{$sL} = 1;
        }
    }
    elsif ($sampList) {
        $sampLists{$sampList} = 1;
    }
    return sort keys %sampLists if keys %sampLists;
    return sort keys %{ $self->_filterSampLists };
}

sub doseFilterSamp {
    my $self           = shift;
    my %args           = @_;
    my $dm             = $self->dm;
    my $doseKeepSampPL = $self->doseKeepSampPL;
    my $dose           = $args{dose} || confess;

    my @sampLists = (
        $self->_getFilterSampLists(
            sample   => $args{filterSample},
            sampList => $args{filterSampList}
        )
    );

    confess " need sample lists " unless @sampLists;

    my $filterDose = basename( $dose, ".dose.gz" ) . ".intersectSamp.dose.gz";
    my $cmd = "gzip -dc $dose ";
    for my $sL (@sampLists) {
        $cmd .= " | $doseKeepSampPL -s $sL ";
    }
    $cmd .= "| gzip -c > $ filterDose ";
    $dm->addRule( $filterDose, [ $dose, @sampLists ], $cmd );

    return $filterDose;
}

sub hap2dose {
    my $self = shift;
    my %args = @_;
    my $dm   = $self->dm;

    my $hap      = $args{hap}      || confess;
    my $keepSnps = $args{keepSnps} || 0;
    my $sample   = $args{sample}   || confess;

    my $sampList = $self->sample2sampleList($sample);

    my $dose = basename( $hap, ".hap.gz" ) . ".dose.gz";
    my @pre = ( $hap, $sampList );

    # sort out markers
    my $cmd =
      "gzip -dc $hap" . q/ | / . $self->hapGen2dosePL . " -s $sampList -h";

    #    $cmd .= " | head -3 ";
    $cmd =
      $self->_filterDose( cmd => $cmd, pre => \@pre, keepSnps => $keepSnps );
    $cmd .= " | gzip -c > $dose.tmp" . " && mv $dose.tmp $dose";

    $dm->addRule( $dose, [@pre], $cmd );

    return $dose;

}

sub gen2dose {

    my $self = shift;
    my %args = @_;
    my $dm   = $self->dm;

    my $gen      = $args{gen}      || confess;
    my $keepSnps = $args{keepSnps} || 0;
    my $sample   = $args{sample}   || confess;
    my $sampList = $self->sample2sampleList($sample);
    my $dose = basename( $gen, ".gen.gz" ) . ".dose.gz";

    my @pre = ( $gen, $sampList );

    # sort out markers
    my $cmd =
      "gzip -dc $gen" . q/ | / . $self->hapGen2dosePL . " -s $sampList -g";


    #$cmd .= " | head -3 ";
    $cmd =
      $self->_filterDose( cmd => $cmd, pre => \@pre, keepSnps => $keepSnps );
    $cmd .= " | gzip -c > $dose.tmp " . " && mv $dose.tmp $dose";

    $dm->addRule( $dose, [@pre], $cmd );

    return $dose;

}

sub vcf2dose {

    my $self       = shift;
    my %args       = @_;
    my $dm         = $self->dm;
    my $vcf2dosePL = $self->vcf2dosePL;
    my $keepSamps  = $args{keepSamps};
    my $vcf        = $args{vcf} || confess;
    my $keepSnps   = $args{keepSnps};
    my $hardCall   = $args{hardCall} || 0;
    my @pre;

    if ($keepSamps) {
        my @sampLists = (
            $self->_getFilterSampLists(
                sample   => $args{filterSample},
                sampList => $args{filterSampList}
            )
        );
        croak " only one sample input file allowed for vcf2dose "
          if @sampLists > 1;

        my $name   = basename( $vcf, ".vcf.gz" );
        my $newVCF = "$name.sampFiltered.vcf.gz";
        my $cmd    = "vcftools --gzvcf $vcf --out $name --recode ";
        for my $sampList (@sampLists) {
            $cmd .= "--keep $sampList";
        }
        $cmd .= " && bgzip -f $name.recode.vcf ";
        $cmd .= " && mv -f $name.recode.vcf.gz $newVCF ";

        my $rhVcfs = $self->_vcfs;
        unless ( exists $rhVcfs->{$newVCF} ) {
            $dm->addRule( $newVCF, [ $vcf, @sampLists ], $cmd );
            $rhVcfs->{$newVCF} = 1;
        }
        $vcf = $newVCF;
    }

    push @pre, $vcf;
    my $filteredDose = basename( $vcf, ".vcf.gz" );
    $filteredDose .= ".HC" if $args{hardCall};
    $filteredDose .= ".dose.gz";

    my $cmd = " gzip -dc $vcf" . " | $vcf2dosePL ";
    $cmd .= " -p GT -h " if $args{hardCall};

    $cmd = $self->_filterDose(
        cmd        => $cmd,
        pre        => \@pre,
        keepSnps   => $keepSnps,
        sampFilter => 0
    );

    $cmd .= " | gzip -c > $filteredDose.tmp "
      . " && mv $filteredDose.tmp $filteredDose";

    $dm->addRule( $filteredDose, \@pre, $cmd );

    return $filteredDose;
}

sub _filterDose {

    my $self           = shift;
    my %args           = @_;
    my $doseKeepSnpsPL = $self->doseKeepSnpsPL;
    my $doseKeepSampPL = $self->doseKeepSampPL;
    my $cmd            = $args{cmd} || confess;
    my $raPre          = $args{pre};
    my $keepSnps       = $args{keepSnps};
    my $sampFilter     = defined $args{sampFilter} ? $args{sampFilter} : 1;

    if ($sampFilter) {
        my @filterSampLists = (
            $self->_getFilterSampLists(
                sample   => $args{filterSample},
                sampList => $args{filterSampList}
            )
        );

        if (@filterSampLists) {
            for my $filterSampList (@filterSampLists) {
                push @{$raPre}, $filterSampList;
                $cmd .= " | $doseKeepSampPL -s $filterSampList";
            }
        }
    }

    my @filterSiteLists = sort keys %{ $self->_filterSiteLists };
    if (@filterSiteLists) {
        for my $list (@filterSiteLists) {
            $cmd .= " | $doseKeepSnpsPL -l $list";
            push @{$raPre}, $list;
        }
    }
    else {
        $cmd .= " | $doseKeepSnpsPL" if $keepSnps;
    }
    return $cmd;
}

no Moose;
__PACKAGE__->meta->make_immutable;

__END__
# Below is stub documentation for your module. You'd better edit it!

=head1 NAME

Dose::Tools - Perl extension for blah blah blah

=head1 SYNOPSIS

   use Dose::Tools;
   blah blah blah

=head1 DESCRIPTION

Stub documentation for Dose::Tools, 
created by template.el.

It looks like the author of the extension was negligent
enough to leave the stub unedited.

Blah blah blah.

=head2 EXPORT

None by default.

=head1 SEE ALSO

Mention other useful documentation such as the documentation of
related modules or operating system documentation (such as man pages
in UNIX), or any relevant external documentation such as RFCs or
standards.

If you have a mailing list set up for your module, mention it here.

If you have a web site set up for your module, mention it here.

=head1 COPYRIGHT AND LICENSE

Copyright (C) 2013 by Warren Winfried Kretzschmar

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself, either Perl version 5.8.2 or,
at your option, any later version of Perl 5 you may have available.

=head1 BUGS

None reported... yet.

=cut

## Please see file perltidy.ERR
