#!perl -T

use Test::More tests => 1;

BEGIN {
    use_ok( 'DoseTools' ) || print "Bail out!\n";
}

diag( "Testing DoseTools $DoseTools::VERSION, Perl $], $^X" );
